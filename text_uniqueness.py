#!/usr/bin/env python3
# coding: utf-8


import re


def norm_text(text):
    text = text.lower()
    text = re.findall(r'[абвгдеёжзийклмнопрстуфхцчшщъыьэюя]+', text)
    return ''.join(text)


def get_bow(text):
    word_list = norm_text(text).split(" ")
    bow = frozenset(word_list)
    return bow


def propotion_of_size_difference(flen, slen):
    m = min(flen, slen)
    p = abs(flen - slen)/m
    return p


def text_is_unique(item_bow, loaded_bows, goal_proportion):
    ilen = len(item_bow)
    for bow in loaded_bows:
        blen = len(bow)
        if propotion_of_size_difference(ilen, blen) > goal_proportion:
            continue
        common = item_bow & bow
        clen = len(common)
        if clen < 1:
            continue
        if propotion_of_size_difference(clen, ilen) < goal_proportion and propotion_of_size_difference(clen, blen) < goal_proportion:
            return False
    return True
