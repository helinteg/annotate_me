#!/usr/bin/env python3
# coding: utf-8


import json
import argparse
import os
from text_uniqueness import get_bow, text_is_unique
import shelve
import operator
from termcolor import colored


id_field = "id"
text_field = "text"
label_field = "label"


def get_loaded_set(path):
    if os.path.isfile(path):
        return set([item[id_field] for item in map(json.loads, open(path))]), set([get_bow(item[text_field]) for item in map(json.loads, open(path))])
    else:
        return set(), set()


def calc_confidence(item):
    proba = item["probability"]
    conf = abs(proba[0] - proba[1])
    return conf


def write(filename, item):
    with open(filename, "a") as out:
        out.write(json.dumps(item, ensure_ascii=False) + "\n")


def get_ids_to_annotate(path_in, shelve_filename, conf_sort):
    id_to_conf = dict()
    c = 0
    with open(path_in, "r") as inf:
        d = shelve.open(shelve_filename)
        for line in inf:
            c = c + 1
            if c % 100000 == 0:
                print(str(c))
            item = json.loads(line)
            conf = 0 if conf_sort is False else calc_confidence(item)
            id_to_conf[item[id_field]] = conf
            d[item[id_field]] = item
        d.close()
    if conf_sort is False:
        return id_to_conf.keys()
    sorted_id_to_conf = sorted(id_to_conf.items(), key=operator.itemgetter(1))
    sorted_id_to_conf = [item[0] for item in sorted_id_to_conf]
    return sorted_id_to_conf


def annotate(path_in, path_out, fields_to_print, marks_list, conf_sort):
    loaded_ids, loaded_bows = get_loaded_set(path_out)

    keys = dict()
    for m in range(len(marks_list)):
        keys[str(m)] = marks_list[m]
    skip_symbol = str(len(marks_list))
    quit_symbol = "q"
    keys[skip_symbol] = "skip"
    keys[quit_symbol] = "quit"
    extra_symbols = set(list(sorted(keys.keys()))[-(len(keys)-len(marks_list)):len(keys)])
    legend = ""
    for k in sorted(keys.keys()):
        legend += k + " for " + keys[k] + ", "
    legend = legend[0:-2]
    print(legend)
    goal_proportion = 0.1

    shelve_filename = path_out + ".shelve"
    ids_to_annotate = get_ids_to_annotate(path_in, shelve_filename, conf_sort)
    keys_set = set(keys.keys())

    for id in ids_to_annotate:
        d = shelve.open(shelve_filename)
        item = d[id]
        d.close()
        item_id = item[id_field]
        item_bow = get_bow(item[text_field])
        if len(item_bow) < 1:
            continue
        if item_id not in loaded_ids and text_is_unique(item_bow, loaded_bows, goal_proportion):
            text_to_print = ""
            for field in fields_to_print:
                text_to_print += field
                text_to_print += ": "
                text_to_print += str(item[field])
                text_to_print += "\n"
            print("\n\n" + text_to_print)
            marks = None
            while True:
                marks_raw = input(colored(legend, "green") + "\n")
                marks = marks_raw.replace(" ", "").split(",")
                curr_marks_set = set(marks)
                if len(curr_marks_set - keys_set) != 0:
                    continue
                if (len(curr_marks_set) != 1) and (len((extra_symbols & curr_marks_set)) != 0):
                    continue
                break
            if marks[0] == quit_symbol:
                print("quit")
                return 0
            if marks[0] == skip_symbol:
                print("SKIPPED")
                continue
            marks = [int(mark) for mark in marks]
            item[label_field] = marks
            write(path_out, item)
            loaded_ids.add(item_id)
            loaded_bows.add(item_bow)
            print("you annotate {0} items".format(len(loaded_ids)))
        else:
            loaded_bows.add(item_bow)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--inf')
    parser.add_argument('--outf')
    parser.add_argument('--marks')
    parser.add_argument('--id_field')
    parser.add_argument('--text_field')
    parser.add_argument('--label_field')
    parser.add_argument('--sort_by_confidence', action='store_true')
    parser.add_argument('--fields_to_print')

    a = parser.parse_args()

    if a.id_field:
        id_field = a.id_field
    if a.text_field:
        text_field = a.text_field
    if a.label_field:
        label_field = a.label_field

    marks_list = a.marks.split(",")
    fields_to_print = a.fields_to_print.split(",")
    annotate(a.inf, a.outf, fields_to_print, marks_list, a.sort_by_confidence)
